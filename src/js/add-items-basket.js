$( document ).ready(function() {

    /*var curCounter = parseInt($('.curCounter').text());*/
    $( ".minus-item" ).click(function() {
        var curCounter = parseInt($(this).siblings(".cur-item").children('.curCounter').text());
        if (curCounter <= 1) {
            $(this).siblings(".cur-item").children('.curCounter').text(1);
        } else {
            $(this).siblings(".cur-item").children('.curCounter').text(--curCounter);
        }
    });
    $( ".plus-item" ).click(function() {
        var curCounter = parseInt($(this).siblings(".cur-item").children('.curCounter').text());
        $(this).siblings(".cur-item").children('.curCounter').text(++curCounter);
    });

});