$( document ).ready(function() {
       $('.product-slider_hide').slick({
        vertical: true,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: false,
        autoplay: true,
        autoplaySpeed: 1000
    }); // small slider for catalog.html

    $('.main-slider').slick ({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        autoplay: false,
        autoplaySpeed: 2000,
        fade: true,
        cssEase: 'linear'
    });// big slider for index.html

    $('.discounted-slider').slick ({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        autoplay: false
    });// slider for news-discount block

});