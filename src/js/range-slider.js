$( document ).ready(function() {
    $( function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 0,
            max: 10000,
            values: [ 0, 10000 ],
            slide: function( event, ui ) {
                $( "#amount-from" ).val( ui.values[ 0 ] );
                $( "#amount-to" ).val( ui.values[ 1 ] );
            }
        });
        $( "#amount-from" ).val( $( "#slider-range" ).slider( "values", 0 ) );
        $( "#amount-to" ).val( $( "#slider-range" ).slider( "values", 1 ) );
    } );


    var filTitle = $('.filter-category__title');
    var filDetail = $('.filter-category__detail');

    $(filTitle).click(function() {
        console.log($(this).siblings($(filDetail)));
        if ( ($(this).siblings($(filDetail))).is(":visible")) {
            $(this).siblings($(filDetail)).hide();
            $(this).children($('.sort-arrow')).css('transform', 'rotate(180deg)');
        } else {
            $(this).siblings($(filDetail)).show();
            $(this).children($('.sort-arrow')).css('transform', 'rotate(360deg)');
        }
    });
});